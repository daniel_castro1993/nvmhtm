#include "nvmhtm.h"
#include "log.h"
#include "tm.h"

#include <cstdio>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <mutex>
#include <map>
#include <vector>
#include <algorithm>
#include <climits>
#include <utility>
#include <dirent.h>

#define ROOT_FILE "./libnvmhtm.root.state"
#define THR_LOG_FILE "./libnvmhtm.thr"
#define S_INSTANCE_EXT ".state"
#define CHECKPOINT_EXT ".ckp%i"

/**
 * TODO: describe the file,
 * 
 * Logs are created per thread, and not per pool.
 */

#define NVMHTM_CHKP_COND(log) ({ \
    LOG_remaining_size(log) < NVMHTM_START_CHECKPOINT * (double) NVMHTM_LOG_SIZE; \
})

#define NVMHTM_CHKP_CRITICAL_COND(log) ({ \
    LOG_remaining_size(log) < NVMHTM_CHECKPOINT_CRITICAL * (double) NVMHTM_LOG_SIZE; \
})

using namespace std;

/* A memory pool is composed as follows:
 * 
 * | 4/8 bytes id | size_given pool |
 * 
 * the id is used to manage different pools.
 */

#define GET_ID(ptr) ({ \
    ID_TYPE *new_ptr = (ID_TYPE*)ptr; \
    new_ptr -= 1; \
    *new_ptr; \
})

#define SET_ID(ptr, id) ({ \
    ID_TYPE *new_ptr = (ID_TYPE*)ptr; \
    new_ptr -= 1; \
    *new_ptr = id; \
})

// TODO: add optional volatile memory
#define ALLOC_WITH_ID(ptr, file, size, is_vol) ({ \
    if (is_vol) { \
        ptr = ALLOC_MEM(file, size + sizeof (ID_TYPE)); \
    } else { \
        ptr = ALLOC_MEM(file, size + sizeof (ID_TYPE)); \
    } \
    ID_TYPE *new_ptr = (ID_TYPE*)ptr; \
    *new_ptr = 1; \
    new_ptr++; \
    ptr = (void*) new_ptr; \
    size; \
})

struct NVMHTM_root_ {
    int pool_counter;
};

enum FLAGS {
    EMPTY = 0, IN_RECOVERY = 1
};

// set_threads is set in init_thrs (before launching the threads)
// nb_thrs is a counter to give ids
static int nb_thrs, set_threads;
static map<thread::id, int> threads;
static vector<NVLog_s*> thr_logs;
static map<ID_TYPE, NVMHTM_mem_s*> s_instance;
static NVMHTM_root_s *s_root;

mutex mtx;

// create a delete_thr, and handle logs

void* NVMHTM_alloc(const char *file_name, size_t size_granules, int vol_pool) {
    int i;
    NVMHTM_mem_s *instance;
    void *pool;
    char chkp_file[512];
    char state_file[512];
    size_t size = size_granules * sizeof (GRANULE_TYPE);

    // mem_root
    if (s_root == NULL) {
        s_root = (NVMHTM_root_s*) ALLOC_MEM(ROOT_FILE, sizeof (NVMHTM_root_s));
    }

    // instance
    sprintf(state_file, "%s" S_INSTANCE_EXT, file_name);
    instance = (NVMHTM_mem_s*) ALLOC_MEM(state_file, sizeof (NVMHTM_mem_s));

    if (instance->id == 0) {
        s_root->pool_counter++;
        NVM_PERSIST(s_root, sizeof (NVMHTM_root_s));
        instance->id = s_root->pool_counter;
    }

    // mem_pool also adds the extra id
    ALLOC_WITH_ID(pool, file_name, size, vol_pool);
    instance->size = size;
    SET_ID(pool, instance->id);
    instance->ptr = pool;
    strcpy(instance->file_name, file_name);

    // Checkpoint
    sprintf(chkp_file, "%s" CHECKPOINT_EXT, file_name,
        (int) (instance->chkp_counter));
    instance->chkp.ptr = ALLOC_MEM(chkp_file, size);
    instance->chkp.size = size;

    s_instance[instance->id] = instance;

    // TODO: bug with the logs
    //    if (vol_pool) {
    //        NVMHTM_recover(pool);
    //    }

    return pool;
}

void NVMHTM_thr_init() {
    int my_tid;
    thread::id this_id = this_thread::get_id();

    // gather an id, from 0 to nb_thrs to the current thread
    my_tid = __sync_fetch_and_add(&nb_thrs, 1);
    threads[this_id] = my_tid;
}

void NVMHTM_init_thrs(int nb_threads) {
    int i;

    // logs
    for (i = 0; i < nb_threads; ++i) {
        thr_logs.push_back(LOG_alloc(i, THR_LOG_FILE, i));
    }

    set_threads = nb_threads;
}

int NVMHTM_get_thr_id() {
    thread::id this_id = this_thread::get_id();
    return threads[this_id];
}

void NVMHTM_clear() {
    map<ID_TYPE, NVMHTM_mem_s*>::iterator it,
        begin = s_instance.begin(),
        end = s_instance.end();
    int i;

    for (i = 0; i < nb_thrs; ++i) {
        NVLog_s *log = thr_logs[i];
        LOG_clear(log);
    }

    for (it = begin; it != end; ++it) {
        NVMHTM_mem_s *instance = it->second;
        memset(instance->ptr, 0, instance->size);
        memset(instance->chkp.ptr, 0, instance->size);
    }
}

void NVMHTM_write(GRANULE_TYPE *addr, GRANULE_TYPE value) {
    GRANULE_TYPE old_val = *addr;
    thread::id this_id = this_thread::get_id();

    *addr = value;

    int id = threads[this_id]; // TODO: find other way of obtaining the ID
    NVLog_s *log = thr_logs[id];

    LOG_push_addr(log, addr, value, old_val);
}

int NVMHTM_has_writes() {
    thread::id this_id = this_thread::get_id();
    int id = threads[this_id];
    NVLog_s *log = thr_logs[id];

    return LOG_has_new_writes(log);
}

int NVMHTM_write_ts(int ts) {
    thread::id this_id = this_thread::get_id();
    int id = threads[this_id];
    NVLog_s *log = thr_logs[id];

    LOG_push_ts(log, ts);
    return ts;
}

void NVMHTM_commit(int read_only) {
    thread::id this_id = this_thread::get_id();
    int id = threads[this_id];
    NVLog_s *log = thr_logs[id];

    // TODO: if read only, do not need to flush
    LOG_flush(log);

#ifndef DISABLE_VALIDATION
    if (!read_only) {
        NVMHTM_validate();
    }
#endif

    LOG_push_commit(log);
    LOG_flush_last_entry(log);

#ifndef DISABLE_DRAIN
    LOG_drain(log);
#endif

    // TODO: first put it to work
    //    if (NVMHTM_CHKP_COND(log)) {
    //        NVMHTM_checkpoint(pool);
    //    }

#ifndef DISABLE_VAL_COUNTERS
    TM_inc_local_counter(id);
#endif
}

void NVMHTM_free(void *pool) {
    // TODO: unmap memory, deal with stuff stored1 
}

void NVMHTM_copy_to_checkpoint(void *pool) {
    // TODO
    NVMHTM_mem_s *instance = s_instance[GET_ID(pool)];
    int i;

    mtx.lock();

    memcpy(instance->chkp.ptr, instance->ptr, instance->size);
    NVM_PERSIST(instance->chkp.ptr, instance->size);

    for (i = 0; i < nb_thrs; ++i) {
        NVLog_s *log = thr_logs[i];
        LOG_clear(log);
    }

    mtx.unlock();
}

void NVMHTM_checkpoint(void *pool) {
    // TODO
    NVCheckpoint_s tmp_checkpoint;
    thread::id this_id = this_thread::get_id();
    int id = threads[this_id];
    char chkp_file[512];
    int new_chkp;
    NVMHTM_mem_s *instance = s_instance[GET_ID(pool)];
    NVLog_s *log = thr_logs[id];
    NVLog_s * logs[nb_thrs];

    copy(thr_logs.begin(), thr_logs.end(), logs);

    if (!mtx.try_lock()) {
        // someone is doing the checkpoint
        if (NVMHTM_CHKP_CRITICAL_COND(log)) {
            // must wait
            mtx.lock();
            mtx.unlock();
        }
        return;
    }
    new_chkp = (instance->chkp_counter + 1) % 2; // only 2 files needed

    sprintf(chkp_file, "%s" CHECKPOINT_EXT, instance->file_name, new_chkp);
    tmp_checkpoint.size = instance->chkp.size;
    tmp_checkpoint.ptr = ALLOC_MEM(chkp_file, instance->chkp.size);

    memcpy(tmp_checkpoint.ptr, instance->chkp.ptr, instance->chkp.size);
    LOG_handle_checkpoint(logs, instance, &tmp_checkpoint, new_chkp);

    NVM_PERSIST(tmp_checkpoint.ptr, tmp_checkpoint.size);
    mtx.unlock();
}

int NVMHTM_is_crash(void *pool) {
    int i;
    ID_TYPE id_pool = GET_ID(pool);
    NVMHTM_mem_s *instance = s_instance[id_pool];

    if (instance->flags & IN_RECOVERY) {
        return 1; // still did not recover
    }

    for (i = 0; i < nb_thrs; ++i) {
        NVLog_s *log = thr_logs[i];
        if (LOG_is_crash(log)) {
            return 1;
        }
    }
    return 0;
}

void NVMHTM_recover(void *pool) {
    // TODO
    NVMHTM_mem_s *instance = s_instance[GET_ID(pool)];
    NVLog_s * logs[nb_thrs];

    copy(thr_logs.begin(), thr_logs.end(), logs);

    // before altering the logs: mark state as IN_RECOVERY
    instance->flags |= IN_RECOVERY;
    NVM_PERSIST(&(instance->flags), sizeof (int));

    LOG_fix(logs, nb_thrs);

    NVMHTM_checkpoint(pool);
    // now is just use the checkpoint
    memcpy((char*) instance->ptr, (char*) instance->chkp.ptr,
        instance->size);

    instance->flags &= ~IN_RECOVERY;
    NVM_PERSIST(&(instance->flags), sizeof (int));
}

void NVMHTM_validate() {
    thread::id this_id = this_thread::get_id();
    int id = threads[this_id];
    int i;

    // wait for active uncommitted transactions
    for (i = 0; i < TM_get_nb_threads(); ++i) {
        int local_counter = TM_get_local_counter(i);
        if (i != id && (local_counter & 1)) {
            int other_global = TM_get_global_counter(i);
            int my_global = TM_get_global_counter(id);
            if (other_global < my_global) {
                // the guy is running with a smaller global counter
                // wait
                this_thread::yield();
                --i;
                continue; // the spinning is not very energy efficient
            }
        }
    }
}

void NVMHTM_crash() {
    // TODO
    abort();
}
