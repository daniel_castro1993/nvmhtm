#include "log.h"
#include "nvmhtm.h"

#include <cstdlib>
#include <cstdio>
#include <algorithm>
#include <vector>
#include <cassert>

#define LOGS_EXT ".log%i"

using namespace std;

struct NVLogEntry_ {
    GRANULE_TYPE *addr;
    GRANULE_TYPE oldv, newv;
};

struct NVLog_ {
    int start, end, lastts, intx;
    NVLogEntry_s ptr[NVMHTM_LOG_SIZE];
};

enum LOG_MARKS {
    EMPTY = 0, LOG_TS = 1, LOG_COMMIT
};

static inline int entry_is_ts(NVLogEntry_s&);
static inline bool entry_is_commit(NVLogEntry_s&);
static inline int ptr_mod(int ptr, int inc, int mod);
static inline int distance(int start, int end);
static inline int add_to_merged(vector<NVLogEntry_s> &, NVLog_s*, int, int);
static int last_safe_ts(NVLog_s**, size_t, vector<pair<int, int>>&);
static void LOG_merge_logs(NVLog_s**, size_t, vector<NVLogEntry_s>&,
    vector<int>&, int, int &last_ts);
static void LOG_merge_logs(NVLog_s**, size_t, vector<NVLogEntry_s>&,
    vector<int>&, int&);

static int nb_logs;

NVLog_s* LOG_alloc(int tid, const char *pool_file, int fresh) {
    NVLog_s *new_log;
    char logfile[512];

    sprintf(logfile, "%s" LOGS_EXT, pool_file, tid);
    new_log = (NVLog_s*) ALLOC_MEM(logfile, sizeof (NVLog_s));

    if (fresh) {
        new_log->start = 0;
        new_log->end = 0;
    }

    nb_logs++; // not thread safe
    
    return new_log;
}

void LOG_clear(NVLog_s *log) {
    log->start = log->end = 0;
}

void LOG_start_tx(NVLog_s *log) {
    log->intx++; // odd: is in TX, even: non-tx
    NVM_PERSIST(&(log->intx), sizeof (int));
}

void LOG_end_tx(NVLog_s *log) {
    log->intx++; // odd: is in TX, even: non-tx
    NVM_PERSIST(&(log->intx), sizeof (int));
}

void LOG_advertise_ts(NVLog_s *log) {
    log->lastts = LOG_last_ts(log);
    NVM_FLUSH(&(log->lastts), sizeof (int));
}

int LOG_has_new_writes(NVLog_s *log) {
    int last_entry = ptr_mod(log->end, -1, NVMHTM_LOG_SIZE);

    return !entry_is_commit(log->ptr[last_entry]);
}

void LOG_flush(NVLog_s *log) {
    int last_commit = LOG_last_commit_idx(log);
    int dist_end, dist_end_l;

    dist_end = distance(last_commit, log->end);
    dist_end_l = distance(last_commit, NVMHTM_LOG_SIZE - 1);

    // may have to flush 2 blocks (TODO)
    if (dist_end_l < dist_end && dist_end_l > 0) {
        NVM_FLUSH(&(log->ptr[last_commit]), dist_end_l * sizeof (NVLogEntry_s));
        NVM_FLUSH(&(log->ptr[0]), (dist_end - dist_end_l) * sizeof (NVLogEntry_s));
    } else {
        NVM_FLUSH(&(log->ptr[last_commit]), dist_end * sizeof (NVLogEntry_s));
    }
    NVM_FLUSH(&(log->start), sizeof (int));
    NVM_FLUSH(&(log->end), sizeof (int));
}

void LOG_flush_last_entry(NVLog_s *log) {
    NVM_FLUSH(&(log->ptr[ptr_mod(log->end, -1, NVMHTM_LOG_SIZE)]),
        sizeof (NVLogEntry_s));
}

void LOG_drain(NVLog_s *log) {
    NVM_DRAIN();
}

void LOG_push_addr(NVLog_s *log, GRANULE_TYPE *addr,
    GRANULE_TYPE newv, GRANULE_TYPE oldv) {
    int end = log->end, new_end;

    new_end = ptr_mod(end, 1, NVMHTM_LOG_SIZE);

    log->ptr[end].addr = addr;
    log->ptr[end].oldv = oldv;
    log->ptr[end].newv = newv;
    //	NVM_PERSIST(&(log->ptr[end]), sizeof (NVLogEntry_s));

    log->end = new_end;
    //	NVM_PERSIST(&(log->end), sizeof (int));
}

void LOG_push_ts(NVLog_s *log, int ts) {
    int end = log->end, new_end;

    new_end = ptr_mod(end, 1, NVMHTM_LOG_SIZE);

    log->ptr[end].addr = NULL;
    log->ptr[end].oldv = LOG_TS;
    log->ptr[end].newv = ts;
    //	NVM_PERSIST(&(log->ptr[end]), sizeof (NVLogEntry_s));

    log->end = new_end;
    //	NVM_PERSIST(&(log->end), sizeof (int));
}

void LOG_push_commit(NVLog_s *log) {
    int end = log->end, new_end;

    new_end = ptr_mod(end, 1, NVMHTM_LOG_SIZE);
    log->ptr[end].addr = NULL;
    log->ptr[end].oldv = LOG_COMMIT;
    //	NVM_PERSIST(&(log->ptr[end]), sizeof (NVLogEntry_s));

    log->end = new_end;
    //	NVM_PERSIST(&(log->end), sizeof (int));
}

int LOG_count_txs(NVLog_s *log) {
    int i = log->start, res = 0;

    while (i != log->end) {
        if (entry_is_commit(log->ptr[i])) {
            res++;
        }

        i = ptr_mod(i, 1, NVMHTM_LOG_SIZE);
    }

    return res;
}

int LOG_last_ts(NVLog_s *log) {
    int i = log->end;

    while (i != log->start) {
        int ts_val = entry_is_ts(log->ptr[i]);
        if (ts_val) {
            return ts_val;
        }

        i = ptr_mod(i, -1, NVMHTM_LOG_SIZE);
    }

    return 0;
}

int LOG_last_commit_idx(NVLog_s *log) {
    int i = log->end;

    while (i != log->start) {
        i = ptr_mod(i, -1, NVMHTM_LOG_SIZE); // end is not part of the log
        if (entry_is_commit(log->ptr[i])) {
            return i;
        }
    }

    return log->start;
}

static inline int find_minimum(int *ts, size_t size) {
    int i, min = ts[0], min_i = 0;
    for (i = 1; i < size; ++i) {
        if (min == 0 || ts[i] < min) {
            min = ts[i];
            min_i = i;
        }
    }

    return min_i;
}

int LOG_last_safe_ts(NVLog_s **logs, size_t nb_logs) {
    vector<pair<int, int>> ts;
    return last_safe_ts(logs, nb_logs, ts);
}

int LOG_remove_last_tx(NVLog_s *log) {
    int i = ptr_mod(log->end, -2, NVMHTM_LOG_SIZE), res = 0;

    if (distance(log->start, log->end) < 2) {
        log->start = log->end;
        return 0;
    }

    while (i != log->start) {
        if (entry_is_commit(log->ptr[i])) {
            int j = ptr_mod(i, -1, NVMHTM_LOG_SIZE);
            log->end = ptr_mod(i, 1, NVMHTM_LOG_SIZE);
            // NVM_PERSIST(&(log->end), sizeof(int));
            return log->ptr[j].newv;
        }

        i = ptr_mod(i, -1, NVMHTM_LOG_SIZE);
    }

    return res;
}

int LOG_has_greater_ts(NVLog_s *log, int ts, int* pos) {
    int i = log->end;

    if (pos != NULL && *pos >= 0 && distance(log->start, *pos)
        < distance(log->start, log->end)) {
        // pos has a hint
        i = *pos;
    } else if (pos != NULL) {
        *pos = log->end;
    }

    while (i != log->start) {
        int ts_val = entry_is_ts(log->ptr[i]);
        if (ts_val && (ts < 0 || ts_val > ts)) {
            if (pos != NULL) {
                *pos = i;
            }
            return ts_val;
        }

        i = ptr_mod(i, -1, NVMHTM_LOG_SIZE);
    }

    return 0;
}

int LOG_next_ts(NVLog_s *log, int ts, int *pos) {
    int i = log->start, res = 0;

    if (pos != NULL && *pos >= 0 && distance(log->start, *pos)
        <= distance(log->start, log->end)) {
        // pos has a hint
        i = *pos;
    } else if (pos != NULL) {
        *pos = log->start;
    }

    while (i != log->end) {
        int ts_val = entry_is_ts(log->ptr[i]);
        if (ts_val && ts_val > ts) {
            if (pos != NULL) {
                *pos = i;
            }
            res = ts_val;
            return res;
        }

        i = ptr_mod(i, 1, NVMHTM_LOG_SIZE);
    }

    return res;
}

int LOG_next_consecutive_ts(NVLog_s *log, int ts, int *pos) {
    int res, *res_pos;

    do {
        res = ts;
        res_pos = pos;
        ts = LOG_next_ts(log, ts, pos);
    }    while (ts - res == 1);

    pos = res_pos;
    return res;
}

int LOG_remaining_size(NVLog_s *log) {
    int res = NVMHTM_LOG_SIZE - distance(log->start, log->end);
    return res;
}

int LOG_is_crash(NVLog_s *log) {
    int end = ptr_mod(log->end, -1, NVMHTM_LOG_SIZE);
    if (!entry_is_commit(log->ptr[end]) && log->start != log->end) {
        return 1;
    }
    return 0;
}

void LOG_handle_checkpoint(NVLog_s **logs, NVMHTM_mem_s *instance,
    NVCheckpoint_s *chkp, int new_chkp) {
    int i, ts; // last ts merged
    vector<NVLogEntry_s> merged;
    vector<int> new_start;
    void *unmapme;

    LOG_merge_logs(logs, nb_logs, merged, new_start, ts);

    if (merged.empty()) {
        return;
    }

    for (i = 0; i < merged.size(); ++i) {
        GRANULE_TYPE *ptr = merged[i].addr;
        *ptr = merged[i].newv;
    }

    NVM_PERSIST(chkp->ptr, chkp->size);

    // change checkpoint
    unmapme = instance->chkp.ptr;
    instance->chkp.ptr = chkp->ptr;
    instance->chkp_counter = new_chkp;

    NVM_PERSIST(instance, sizeof (NVMHTM_mem_s));

    // erase logs
    for (i = 0; i < nb_logs; ++i) {
        NVLog_s *log = logs[i];
        log->start = new_start[i];
        // TODO: sometimes this assert breaks
        //        assert(distance(log->start, log->end) == 0 ||
        //               entry_is_commit(log->ptr[ptr_mod(log->start, -1, NVMHTM_LOG_SIZE)]));
        NVM_PERSIST(&(log->start), sizeof (int));
    }

    // delete the previous checkpoint
    FREE_MEM(unmapme, instance->chkp.size);
}

void LOG_fix(NVLog_s **logs, size_t size) {
    vector<pair<int, int>> invalid_ts;
    int i, j;

    last_safe_ts(logs, size, invalid_ts);

    // fix logs with invalid entries
    for (i = 0; i < size; ++i) {
        NVLog_s *log = logs[i];

        if (log->end != log->start) {
            j = ptr_mod(log->end, -1, NVMHTM_LOG_SIZE);
            if (!entry_is_commit(log->ptr[j])) {
                // invalid
                LOG_remove_last_tx(log);
            }
        }
    }

    // remove invalid transactions
    for (i = 0; i < invalid_ts.size(); ++i) {
        LOG_remove_last_tx(logs[invalid_ts[i].first]);
    }
}

static inline int entry_is_ts(NVLogEntry_s &entry) {
    if (entry.addr != nullptr) {
        return 0;
    }

    return entry.oldv == LOG_TS ? entry.newv : 0;
}

static inline bool entry_is_commit(NVLogEntry_s &entry) {
    bool res = entry.addr == nullptr && entry.oldv == LOG_COMMIT;
    return res;
}

static inline int ptr_mod(int ptr, int inc, int mod) {
    int res = ptr;
    res += inc;
    if (res < 0) {
        res += mod;
    } else if (res >= mod) {
        res -= mod;
    }
    return res;
}

static inline int distance(int start, int end) {
    int res = end - start;
    if (res < 0) {
        res += NVMHTM_LOG_SIZE;
    }
    return res;
}

static inline int add_to_merged(vector<NVLogEntry_s> &res,
    NVLog_s *log, int begin, int last) {
    int i;

    for (i = begin; i != last; i = ptr_mod(i, 1, NVMHTM_LOG_SIZE)) {
        int ts_val = entry_is_ts(log->ptr[i]);
        if (log->ptr[i].addr != NULL) {
            res.push_back(log->ptr[i]);
        } else if (ts_val) {
            break;
        }
    }

    // next update, after TS and COMMIT
    return i == begin ? i : ptr_mod(i, 2, NVMHTM_LOG_SIZE);
}

static int last_safe_ts(NVLog_s **logs, size_t nb_logs,
    vector<pair<int, int>> &invalid_ts) {
    int i, j, res;
    vector<pair<int, int>> ts;

    invalid_ts.clear();

    for (i = 0; i < nb_logs; ++i) {
        NVLog_s *log = logs[i];
        if (log->end == log->start) {
            continue;
        }
        j = log->start;
        while (j != log->end) {
            int ts_val = entry_is_ts(log->ptr[j]);
            if (ts_val) {
                ts.push_back(make_pair(i, ts_val)); // add last ts
            }
            j = ptr_mod(j, 1, NVMHTM_LOG_SIZE);
        }
    }
    // adds all ts, find a more efficient way
    sort(ts.begin(), ts.end(), [] (pair<int, int> a,
        pair<int, int> b) -> bool {
            return a.second < b.second;
        }); // sorted like [1, 2, 3, 4]
    for (i = 1; i < ts.size(); ++i) {
        if (ts[i].second - ts[i - 1].second != 1) {

            for (j = i; i < ts.size(); ++i) {
                invalid_ts.push_back(ts[i]);
            }

            // if [1, 2, 4] then return 2 (4 is unsafe)
            return ts[i - 1].second; // returns the ts without "holes"
        }
    }
    return ts.empty() ? 0 : ts[i - 1].second;
}

static void LOG_merge_logs(NVLog_s **logs, size_t size,
    vector<NVLogEntry_s> &res,
    vector<int> &vbegin,
    int truncate_at,
    int &last_ts) {
    vector<pair<int, int>> vlogs;
    int i, j, k;
    unsigned int exit = 0;

    vbegin.clear();

    // bootstrap vlogs with minimum ts
    for (i = 0; i < size; ++i) {
        NVLog_s *log = logs[i];
        j = log->start;
        int ts = LOG_next_ts(log, 0, &j);
        vbegin.push_back(log->start);
        vlogs.push_back(make_pair(i, ts));
    }

    while (exit != ((1 << vlogs.size()) - 1)) { // TODO: change the exit condition (do not use bits)
        // sorts logs
        sort(vlogs.begin(), vlogs.end(), [] (pair<int, int> a,
            pair<int, int> b) -> bool {
                return a.second < b.second;
            });

        for (i = 0; i < vlogs.size(); ++i) {
            if (vlogs[i].second == 0 || vlogs[i].second > truncate_at) {
                exit |= 1 << i; // set the bit for this thread on
            } else {
                last_ts = vlogs[i].second;
                j = vlogs[i].first; // thrd_id with last_ts
                NVLog_s *log = logs[j];

                // adds to the global log
                k = vbegin[j];
                k = add_to_merged(res, log, k, log->end);
                vbegin[j] = k;

                // grabs the next ts
                vlogs[i].second = LOG_next_ts(log, vlogs[i].second, &k);
                break;
            }
        }
    }
}

void static LOG_merge_logs(NVLog_s **logs, size_t size,
    vector<NVLogEntry_s> &res,
    vector<int> &new_start,
    int &last_ts) {
    int ts = LOG_last_safe_ts(logs, size);
    if (ts == 0) {
        // can be the case of a recent checkpoint exception
        res.clear();
        new_start.clear();
        last_ts = 0;
        return;
    }
    LOG_merge_logs(logs, size, res, new_start, ts, last_ts);
}
