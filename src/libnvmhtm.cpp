#include "libnvmhtm.h"

#include <stdio.h>
#include <stdlib.h>
#include <ctime>

static void* pool;

static clock_t ts_init, ts_shutdown;

void* LIBNVMHTM_alloc(const char *file_name, size_t size, int vol_pool)
{
    pool = NVMHTM_alloc(file_name, size, vol_pool);

    return pool;
}

void LIBNVMHTM_init(int nb_threads)
{
    TM_init_nb_threads(nb_threads);
    NVMHTM_init_thrs(nb_threads);
    ts_init = clock(); // TODO: statistics optional
}

void LIBNVMHTM_thr_init()
{
    NVMHTM_thr_init();
    int tid = NVMHTM_get_thr_id(); // TODO: statistics optional
    TM_set_is_record(tid, 1);
}

void LIBNVMHTM_thr_exit()
{
    int tid = NVMHTM_get_thr_id(); // TODO: statistics optional
    int local_counter = TM_get_local_counter(tid);
    if (local_counter & 1) {
        // BUGGED
        TM_inc_local_counter(tid);
    }
}

void LIBNVMHTM_abort_tx()
{
    TM_abort();
}

void LIBNVMHTM_clear()
{
    NVMHTM_clear();
}

void LIBNVMHTM_checkpoint()
{
    NVMHTM_copy_to_checkpoint(pool);
}

void LIBNVMHTM_after_commit(int ts)
{
    int read_only = !NVMHTM_has_writes();
    NVMHTM_write_ts(ts);
    NVMHTM_commit(read_only);
}

void LIBNVMHTM_write(GRANULE_TYPE *addr, GRANULE_TYPE value)
{
#ifndef DISABLE_LOG
    NVMHTM_write(addr, value);
#else
    *addr = value;
#endif
}

void LIBNVMHTM_shutdown()
{
    // TODO: statistics optional
    clock_t total_time;
    double time_taken;
    
    ts_shutdown = clock();    
    total_time = ts_shutdown - ts_init;
    time_taken = (double) total_time / (double) CLOCKS_PER_SEC;
            
    int successes = TM_get_error_count(SUCCESS);
    int fallbacks = TM_get_error_count(FALLBACK);
    int aborts = TM_get_error_count(ABORT);
    int conflicts = TM_get_error_count(CONFLICT);
    int capacities = TM_get_error_count(CAPACITY);
    int commits = successes + fallbacks;
    double X = (double) commits / (double) time_taken;
    double P_A = (double) aborts / (double) (successes + aborts);
    
    printf("Time %f s\n", time_taken);
    printf("COMMIT : %i\n", successes);
    printf("ABORTS : %i\n", aborts);
    printf("CONFLS : %i\n", conflicts);
    printf("CAPACS : %i\n", capacities);
    printf("FALLBK : %i\n", fallbacks);
    printf("     X : %f\n", X);
    printf("   P_A : %f\n", P_A);
}
