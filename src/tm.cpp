#include "tm.h"

#include <cstdlib>
#include <thread>
#include <mutex>

#ifndef TM_INIT_BUDGET
#define TM_INIT_BUDGET 5
#endif

using namespace std;

static bool *is_locked;
static mutex mtx;
static int global_counter;
static int count_serl_txs;
static int init_budget = TM_INIT_BUDGET;
static int threads;

static int *is_record;
static int **errors;

tx_counters_s **htm_tx_val_counters; // cache alligned

void TM_check_global_lock(int is_in_tx)
{
    if (*is_locked) { // reads the SGL, aborts on write conflict
        if (is_in_tx) {
            NVMHTM_ABORT();
        }
        else {
            mtx.lock(); // blocks the transaction and waits the serl. one
            mtx.unlock();
        }
    }
}

void TM_init_nb_threads(int nb_threads)
{
    int i;
    threads = nb_threads;

    is_locked = (bool*) malloc(sizeof (bool));
    *is_locked = false;

    is_record = (int*) malloc(sizeof (int) * nb_threads);
    errors = (int**) malloc(sizeof (int*) * nb_threads);
    for (i = 0; i < nb_threads; ++i) {
        errors[i] = (int*) malloc(sizeof (int) * SIZEOF_ERRORS_T);
        memset(errors[i], 0, sizeof (int) * SIZEOF_ERRORS_T);
    }

    // TODO: does not work in older systems
    htm_tx_val_counters = (tx_counters_s**)
            malloc(sizeof (tx_counters_s*) * nb_threads);

    for (i = 0; i < nb_threads; ++i) {
        htm_tx_val_counters[i] = (tx_counters_s*)
                aligned_alloc(CACHE_LINE_SIZE, sizeof (tx_counters_s));
        memset(htm_tx_val_counters[i], 0, sizeof (tx_counters_s));
    }
}

int TM_get_budget(int)
{
    return init_budget;
}

void TM_set_budget(int _budget)
{
    init_budget = _budget;
}

void TM_enter_fallback(int tid)
{
    mtx.lock();
    *is_locked = true;
    errors[tid][FALLBACK]++;
}

void TM_exit_fallback(int)
{
    *is_locked = false;
    mtx.unlock();
}

void TM_ex_lock()
{
    mtx.lock();
    *is_locked = true;
}

void TM_ex_unlock()
{
    *is_locked = false;
    mtx.unlock();
}

void TM_abort()
{
    NVMHTM_ABORT();
}

void TM_set_is_record(int tid, int is_rec)
{
    is_record[tid] = is_rec;
}

int TM_update_budget(int tid, int budget, TM_STATUS_TYPE error)
{
    int res = 0;

    if (is_record[tid]) {
        ERROR_INC(error, errors[tid]);
    }

    if (budget > 0) {
        res = budget - 1;
    }

    return res;
}

int TM_get_error_count(int error)
{
    int i, res = 0;
    for (i = 0; i < threads; ++i) {
        res += errors[i][error];
    }
    return res;
}

int TM_inc_global_counter(int tid)
{
    htm_tx_val_counters[tid]->global_counter = ++global_counter;
    return htm_tx_val_counters[tid]->global_counter;
}

void TM_inc_local_counter(int tid)
{
    (htm_tx_val_counters[tid]->local_counter)++;
}

int TM_get_local_counter(int tid)
{
    return htm_tx_val_counters[tid]->local_counter;
}

int TM_get_global_counter(int tid)
{
    return htm_tx_val_counters[tid]->global_counter;
}

int TM_get_nb_threads()
{
    return threads;
}
