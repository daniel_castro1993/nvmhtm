#!/bin/sh

cd ./build
cmake .. -DUSE_VOL=0 -DUSE_VALIDATION=0 -DDISABLE_LOG=1
make

cd ../bin/ 
rm -f *.txt
rm -f bank_accounts.dat*

for t in 2 4 6 8 10 12 14
do 
    for a in 256 512 1024 2048
    do 
        ./nvmhtm_test THREADS $t NB_ACCOUNTS $a GNUPLOT_FILE "no_logs.ACC_$a.txt" ;
        wait ; wait ;
        rm -f bank_accounts.dat*
        sleep 0.1 ;
    done
    wait ; wait ;
    sleep 0.1 ;
done

cd ../build
cmake .. -DUSE_VOL=0 -DUSE_VALIDATION=0 -DDISABLE_LOG=0
make

cd ../bin/ 
rm -f bank_accounts.dat*

for t in 2 4 6 8 10 12 14
do 
    for a in 256 512 1024 2048
    do 
        ./nvmhtm_test THREADS $t NB_ACCOUNTS $a GNUPLOT_FILE "logs_no_val.ACC_$a.txt" ;
        wait ; wait ;
        rm -f bank_accounts.dat*
        sleep 0.1 ;
    done
    wait ; wait ;
    sleep 0.1 ;
done

cd ../build
cmake .. -DUSE_VOL=0 -DUSE_VALIDATION=1 -DDISABLE_LOG=0
make

cd ../bin/ 
rm -f bank_accounts.dat*

for t in 2 4 6 8 10 12 14
do 
    for a in 256 512 1024 2048
    do 
        ./nvmhtm_test THREADS $t NB_ACCOUNTS $a GNUPLOT_FILE "logs_val.ACC_$a.txt" ;
        wait ; wait ;
        rm -f bank_accounts.dat*
        sleep 0.1 ;
    done
    wait ; wait ;
    sleep 0.1 ;
done
