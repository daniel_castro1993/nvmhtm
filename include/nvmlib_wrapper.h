#ifndef NVMLIB_WRAPPER_H
#define NVMLIB_WRAPPER_H

#ifdef __cplusplus
extern "C"
{
#endif

    // TODO: add the minimal library
#if USE_MIN_NVM == 1
#include <min_nvm.h>
#else /* USE_MIN_NVM */
#include <libpmem.h>
#include <libvmem.h>
#endif /* USE_MIN_NVM */

#include <string.h>

    // if VOL then the path must be a directory

#define ALLOC_VMEM(objtype, objref, file_name, sizeobj) ({ \
    size_t mapped_len = sizeobj; \
    int is_pmem; \
    VMEM *vmp; \
    if ((vmp = vmem_create("./", VMEM_MIN_POOL)) == NULL) { /* TODO: */ \
                perror("vmem_create"); \
                exit(EXIT_FAILURE); \
        } \
    if ((objref = (objtype*) \
                vmem_malloc(vmp, sizeobj)) == NULL) { \
                perror("vmem_malloc"); \
                exit(EXIT_FAILURE); \
        } \
    memset(objref, 0, sizeobj); \
    mapped_len; \
})

#define ALLOC_PMEM(objtype, objref, file_name, sizeobj) ({ \
    size_t mapped_len; \
    int is_pmem; \
    if (((objref) = (objtype*) \
                pmem_map_file(file_name, sizeobj, \
                              PMEM_FILE_CREATE, 0666, &mapped_len, \
                              &is_pmem)) == NULL) { \
                perror("pmem_map_file"); \
                exit(EXIT_FAILURE); \
        } \
    mapped_len; \
})

#if USE_MIN_NVM == 1

#define ALLOC_MEM(file, size)  MN_alloc(file, size)
#define FREE_MEM(ptr, size)    MN_free(ptr)

#define NVM_PERSIST(ptr, size) MN_flush(ptr, size); MN_drain()
#define NVM_FLUSH(ptr, size)   MN_flush(ptr, size)
#define NVM_DRAIN()            MN_drain()

#else /* USE_MIN_NVM */
#if USE_VOL == 1
#define ALLOC_MEM(file, size) ({ \
    void *res; \
    ALLOC_VMEM(void, res, file, size); \
    res; \
})
#else /* USE_VOL */
#define ALLOC_MEM(file, size) ({ \
    void *res; \
    ALLOC_PMEM(void, res, file, size); \
    res; \
})
#endif /* USE_VOL */

#define FREE_MEM(ptr, size)    pmem_unmap(ptr, size)

#define NVM_PERSIST(ptr, size) pmem_persist(ptr, size)
#define NVM_FLUSH(ptr, size)   pmem_flush(ptr, size)
#define NVM_DRAIN()            pmem_drain()

#endif /* USE_MIN_NVM */

#ifdef __cplusplus
}
#endif

#endif /* NVMLIB_WRAPPER_H */

