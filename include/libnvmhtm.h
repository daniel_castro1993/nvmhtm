#ifndef LIB_NVMHTM_H
#define LIB_NVMHTM_H

#ifdef __cplusplus
extern "C"
{
#endif

#include "nvmhtm.h"

#include <stdlib.h>

#define GRANULE_TYPE long

#if DISABLE_LOG == 1
#define TEST_END() 
#else
#define TEST_END(ts) LIBNVMHTM_after_commit(ts)
#endif
    
#define LIBNVMHTM_start_tx() \
    do { \
        int tid = NVMHTM_get_thr_id(); \
        int ts_var; \
        TM_begin(tid); \

#define LIBNVMHTM_commit_tx() \
        TM_commit(tid, ts_var); \
        TEST_END(ts_var); \
    } while(0); \

    void* LIBNVMHTM_alloc(const char *file_name, size_t size, int use_vol);
    void LIBNVMHTM_init(int nb_threads);
    void LIBNVMHTM_shutdown();
    void LIBNVMHTM_thr_init();
    void LIBNVMHTM_thr_exit();
    void LIBNVMHTM_abort_tx();
    void LIBNVMHTM_clear();
    void LIBNVMHTM_checkpoint();
    
    void LIBNVMHTM_after_commit(int); // do not call this

    void LIBNVMHTM_write(GRANULE_TYPE *addr, GRANULE_TYPE value);

#ifdef __cplusplus
}
#endif

#endif /* LIB_NVMHTM_H */

