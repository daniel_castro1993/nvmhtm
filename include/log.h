#ifndef LOG_H
#define LOG_H

#include "utils.h"

#include <stdlib.h>

#ifdef __cplusplus
extern "C"
{
#endif

    // TODO: the checkpointing is only done after commit, we must be sure
    // that the number of writes within transactions are never greater than:
    // NVMHTM_CHECKPOINT_CRITICAL * NVMHTM_LOG_SIZE

    typedef struct NVCheckpoint_ NVCheckpoint_s;
    typedef struct NVMHTM_mem_ NVMHTM_mem_s;
    typedef struct NVLogEntry_ NVLogEntry_s;
    typedef struct NVLog_ NVLog_s;

    NVLog_s* LOG_alloc(int tid, const char *pool_file, int fresh);
    void LOG_clear(NVLog_s*);
    void LOG_start_tx(NVLog_s*);
    void LOG_end_tx(NVLog_s*);
    void LOG_advertise_ts(NVLog_s*);
    int LOG_has_new_writes(NVLog_s*);
    void LOG_flush(NVLog_s*);
    void LOG_flush_last_entry(NVLog_s*);
    void LOG_drain(NVLog_s*);
    void LOG_push_addr(NVLog_s*, GRANULE_TYPE *addr, 
                       GRANULE_TYPE newv, GRANULE_TYPE oldv);
    void LOG_push_ts(NVLog_s*, int);
    void LOG_push_commit(NVLog_s*);
    int LOG_count_txs(NVLog_s*);
    int LOG_last_ts(NVLog_s*);
    int LOG_last_commit_idx(NVLog_s*);
    int LOG_last_safe_ts(NVLog_s**, size_t);
    int LOG_remove_last_tx(NVLog_s*);
    int LOG_has_greater_ts(NVLog_s*, int, int*);
    int LOG_next_ts(NVLog_s*, int, int*);
    int LOG_next_consecutive_ts(NVLog_s*, int, int*);
    int LOG_remaining_size(NVLog_s*);
    int LOG_is_crash(NVLog_s*);
    void LOG_fix(NVLog_s**, size_t);
    void LOG_handle_checkpoint(NVLog_s**, NVMHTM_mem_s*, NVCheckpoint_s*, int);

#ifdef __cplusplus
}
#endif

#endif /* LOG_H */

