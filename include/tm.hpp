#ifndef TM_GUARD
#define TM_GUARD

// #ifndef __USE_GNU
// #define __USE_GNU
// #endif
// #define _GNU_SOURCE

#include "rdtsc.h"

#include <immintrin.h>
#include <rtmintrin.h>
#include <thread>

#ifndef TM_MAX_BUDGET
#pragma message ( "TM_MAX_BUDGET set to default value of 5" )
#define TM_MAX_BUDGET 5
#endif

/**
 * _XBEGIN_STARTED    -1
 * _XABORT_EXPLICIT   0x01
 * _XABORT_RETRY      0x02
 * _XABORT_CONFLICT   0x04
 * _XABORT_CAPACITY   0x08
 * _XABORT_DEBUG      0x10
 * _XABORT_NESTED     0x20
 */

#ifndef min
#define min(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a < _b ? _a : _b; })
#endif
#ifndef max
#define max(a,b) \
   ({ __typeof__ (a) _a = (a); \
       __typeof__ (b) _b = (b); \
     _a > _b ? _a : _b; })
#endif

#define UPDATE_BUDGET(is_abort, budget) ({ \
  budget = is_abort ? max(budget - 1, 0) : budget; \
})

#define CHECK_GLOBAL_LOCK(lock, mutex, in_xact) ({ \
  if(*lock) { \
    if(in_xact) { \
      _xabort(30); \
    } else { \
      mutex.lock(); \
      mutex.unlock(); \
    } \
  } \
})

#define ENTER_FALLBACK(lock, mutex) ({ \
  mutex.lock(); \
  *lock = true; \
})

#define EXIT_FALLBACK(lock, mutex) ({ \
  *lock = false; \
  mutex.unlock(); \
})

#define START_TRANSAC() do { \
  int budget = TM_MAX_BUDGET; \
  bool *is_locked = &TM_lock; \
  register int status; \
  do { \
    CHECK_GLOBAL_LOCK(is_locked, TM_mtx, false); \
    status = _xbegin(); \
    if (status != _XBEGIN_STARTED) { \
      UPDATE_BUDGET(true, budget) \
    } else { \
      CHECK_GLOBAL_LOCK(is_locked, TM_mtx, true); \

#define END_TRANSAC() \
      _xend(); \
    } \
  } while (budget > 0); \
  if (budget < 1) { \
    ENTER_FALLBACK(is_locked, TM_mtx) \
  } \
} while (false) // handles multiple transactions in same function (does not nested)

bool TM_GSL;
std::mutex TM_mtx;

#endif /* TM_GUARD */
