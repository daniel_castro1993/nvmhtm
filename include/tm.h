#ifndef NEWFILE_H
#define NEWFILE_H

#include "nvmhtm.h"


#if USE_P8 == 1
//#pragma message ( "USING_P8" )
#include <htmxlintrin.h>
#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE 128
#endif
#else
//#pragma message ( "USING_TSX" )
#include <immintrin.h>
#include <rtmintrin.h>
#ifndef CACHE_LINE_SIZE
#define CACHE_LINE_SIZE 64
#endif
#endif

#ifdef __cplusplus
extern "C"
{
#endif

#if USE_P8 == 1

    typedef enum
    {
        SUCCESS = 0, ABORT, EXPLICIT, ILLEGAL, CONFLICT, CAPACITY,
        NESTING_DEPTH, NESTED_TOO_DEEP, OTHER, FALLBACK, PERSISTENT
    } errors_e;
#define SIZEOF_ERRORS_T 11
#define TM_STATUS_TYPE TM_buff_type
#define START_TRANSACTION(var) (__TM_begin(var))
#define CODE_SUCCESS _HTM_TBEGIN_STARTED
#define NVMHTM_ABORT() __TM_abort()
#define NVMHTM_COMMIT() __TM_end()
#else

    typedef enum
    {
        SUCCESS = 0, ABORT, EXPLICIT, RETRY, CONFLICT,
        CAPACITY, DEBUG, NESTED, OTHER, FALLBACK
    } errors_e;
#define SIZEOF_ERRORS_T 10
#define TM_STATUS_TYPE register int
#define START_TRANSACTION(var) (var = _xbegin())
#define CODE_SUCCESS _XBEGIN_STARTED
#define NVMHTM_ABORT() _xabort(30)
#define NVMHTM_COMMIT() _xend()
#endif

#define TM_CHECK_GLOBAL_LOCK(is_in_tx) TM_check_global_lock(is_in_tx)
#define TM_ENTER_FALLBACK(tid) TM_enter_fallback(tid)
#define TM_EXIT_FALLBACK(tid) TM_exit_fallback(tid)

#ifndef DISABLE_VAL_COUNTERS
    // big hack to achieve less aborts
#define TM_SET_COMMIT_COUNTER(tid, cnts, ts_var) \
    (cnts[tid]->local_counter)++; \
    ts_var = ++(cnts[tid]->global_counter)
#else
#define TM_SET_COMMIT_COUNTER(tid, cnts, ts_var) \
    ts_var = ++(cnts[tid]->global_counter) 
#endif

#define TM_begin(tid) \
do { \
    register int TM_budget_var = TM_get_budget(tid); \
    extern tx_counters_s **htm_tx_val_counters; \
    TM_STATUS_TYPE TM_status_var; \
    while (1) { \
        if (TM_budget_var > 0) { \
            TM_CHECK_GLOBAL_LOCK(0); \
            if (START_TRANSACTION(TM_status_var) != CODE_SUCCESS) { \
                TM_budget_var = TM_update_budget(tid, TM_budget_var, TM_status_var); \
                continue; \
            } \
            TM_CHECK_GLOBAL_LOCK(1); \
        } \
        else { \
            TM_ENTER_FALLBACK(tid); \
        }

#define TM_commit(tid, ts_var) \
        if (TM_budget_var > 0) { \
            TM_SET_COMMIT_COUNTER(tid, htm_tx_val_counters, ts_var); \
            NVMHTM_COMMIT(); \
            TM_update_budget(tid, TM_budget_var, TM_status_var); \
        } \
        else { \
            TM_SET_COMMIT_COUNTER(tid, htm_tx_val_counters, ts_var); \
            TM_EXIT_FALLBACK(tid); \
        } \
        break; \
    } \
} \
while (0);

#define TSX_ERROR_INC(status, error_array) ({ \
  if (status == _XBEGIN_STARTED) { \
    error_array[SUCCESS] += 1; \
  } else { \
    error_array[ABORT] += 1; \
    int nb_errors = __builtin_popcount(status); \
    int tsx_error = status; \
    do { \
      int idx = tsx_error & _XABORT_EXPLICIT ? \
        ({tsx_error = tsx_error & ~_XABORT_EXPLICIT; EXPLICIT;}) : \
      tsx_error & _XABORT_RETRY ? \
        ({tsx_error = tsx_error & ~_XABORT_RETRY; RETRY;}) : \
      tsx_error & _XABORT_CONFLICT ? \
        ({tsx_error = tsx_error & ~_XABORT_CONFLICT; CONFLICT;}) : \
      tsx_error & _XABORT_CAPACITY ? \
        ({tsx_error = tsx_error & ~_XABORT_CAPACITY; CAPACITY;}) : \
      tsx_error & _XABORT_DEBUG ? \
        ({tsx_error = tsx_error & ~_XABORT_DEBUG; DEBUG;}) : \
      OTHER; \
      error_array[idx] += 1; \
      nb_errors--; \
    } while (nb_errors > 0); \
  } \
})

#define P8_ERROR_TO_INDEX(tm_buffer) ({ \
  errors_t code = OTHER; \
  if(*_TEXASRL_PTR (tm_buffer) == 0) code = SUCCESS; \
  else if(__TM_is_conflict(tm_buffer)) code = CONFLICT; \
  else if(__TM_is_illegal  (tm_buffer)) code = ILLEGAL; \
  else if(__TM_is_footprint_exceeded(tm_buffer)) code = CAPACITY; \
  else if(__TM_nesting_depth(tm_buffer)) code = NESTING_DEPTH; \
  else if(__TM_is_nested_too_deep(tm_buffer)) code = NESTED_TOO_DEEP; \
  else if(__TM_is_user_abort(tm_buffer)) code = EXPLICIT; \
  else if(__TM_is_failure_persistent(tm_buffer)) code = PERSISTENT; \
  code; \
})
#define P8_ERROR_INC(status, error_array) ({ \
  errors_t code = P8_ERROR_TO_INDEX(status); \
  if (code != SUCCESS) error_array[ABORT] += 1; \
  error_array[code] += 1; \
})

#if USE_P8 == 1
#define ERROR_INC P8_ERROR_INC
#else
#define ERROR_INC TSX_ERROR_INC
#endif

    typedef struct tx_counters_ tx_counters_s;
    // TODO: right now is not possible to mode this to tm.cpp    

    struct __attribute__ ((aligned(CACHE_LINE_SIZE<<8))) tx_counters_
    {
        int local_counter, global_counter;
    }; // if they are not cache line aligned we get MANY aborts

    // don't call these
    // --
    int TM_get_budget(int tid);
    void TM_set_is_record(int tid, int is_rec);
    void TM_set_budget(int budget);
    void TM_check_global_lock(int is_in_tx);
    void TM_enter_fallback(int tid);
    void TM_exit_fallback(int tid);
    int TM_update_budget(int tid, int budget, TM_STATUS_TYPE status);
    void TM_set_commit_counter(int tid);
    // --

    int TM_inc_global_counter(int tid);
    void TM_inc_local_counter(int tid);

    int TM_get_local_counter(int tid);
    int TM_get_global_counter(int tid);
    void TM_init_nb_threads(int threads);
    int TM_get_nb_threads();

    void TM_ex_lock();
    void TM_ex_unlock();

    void TM_abort();

    // statistics
    int TM_get_error_count(int);

#ifdef __cplusplus
}
#endif

#endif /* NEWFILE_H */

