#ifndef UTILS_H
#define UTILS_H

#ifdef __cplusplus
extern "C"
{
#endif

#ifndef NVMHTM_LOG_SIZE
#define NVMHTM_LOG_SIZE 256
#endif

#ifndef TM_INIT_BUDGET
#define TM_INIT_BUDGET 5
#endif

#ifndef MAX_NB_THREADS
#define MAX_NB_THREADS 64
#endif

#ifndef GRANULE_TYPE
#define GRANULE_TYPE long
#endif

#ifdef __cplusplus
}
#endif

#endif /* UTILS_H */

