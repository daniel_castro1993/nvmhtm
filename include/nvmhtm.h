#ifndef NVMHTM_H
#define NVMHTM_H 1

#ifdef __cplusplus
extern "C"
{
#endif

#ifdef mode_t
#undef mode_t
#endif

#include "log.h"
    
#include "tm.h"
#include "nvmlib_wrapper.h"

#ifndef GRANULE_TYPE
#define GRANULE_TYPE long
#endif

    // TODO: add deltas to the log to save space
#define ADDR_TO_DELTA(base, addr) ({ \
        ((GRANULE_TYPE*) addr - (GRANULE_TYPE*) base) + VALID_ADDRS; \
}) // Delta from the beginning of the pool

#define DELTA_TO_ADDR(base, delta) ({ \
        ((GRANULE_TYPE*) base + delta) - VALID_ADDRS; \
})

#ifndef NVMHTM_MAX_NB_THREADS
#define NVMHTM_MAX_NB_THREADS 64
#endif
    
    // ratio of the log occupied to start checkpointing (only do this on commit)
#ifndef NVMHTM_START_CHECKPOINT
#define NVMHTM_START_CHECKPOINT 0.3 
#endif

    // logs are almost filled, must wait for the checkpoint
#ifndef NVMHTM_CHECKPOINT_CRITICAL
#define NVMHTM_CHECKPOINT_CRITICAL 0.1 
#endif


#define ID_TYPE GRANULE_TYPE

    typedef unsigned long ts_s;

    typedef struct NVCheckpoint_ NVCheckpoint_s;
    typedef struct NVMHTM_mem_ NVMHTM_mem_s;
    typedef struct NVMHTM_root_ NVMHTM_root_s;

    struct NVCheckpoint_
    {
        size_t size;
        void *ptr;
    };

    struct NVMHTM_mem_
    {
        NVCheckpoint_s chkp;
        ts_s ts, chkp_counter;
        ID_TYPE id;
        void *ptr;
        size_t size;
        char file_name[256]; // TODO: check the size
        int flags;
    };

    /**
     * 
     * @param file_name file where the shared pool will be stored
     * @param size number of granules (in sizeof (GRANULE_TYPE)) to allocate
     * @return 
     */
    void NVMHTM_init(int thrs);
    void* NVMHTM_alloc(const char* file_name, size_t, int vol_pool);
    void NVMHTM_thr_init(); // call this from within the thread
    void NVMHTM_init_thrs(int nb_threads);
    int NVMHTM_get_thr_id();
    
    // sets to 0 all allocated memory, checkpoints and logs
    void NVMHTM_clear();
    void NVMHTM_write(GRANULE_TYPE *addr, GRANULE_TYPE value); // TODO: remove the pool
    int NVMHTM_write_ts(int);
    int NVMHTM_has_writes();
    void NVMHTM_commit(int read_only);
    void NVMHTM_copy_to_checkpoint(void*);
    void NVMHTM_checkpoint(void*);
    int NVMHTM_is_crash(void*);
    void NVMHTM_recover(void*);
    void NVMHTM_free(void*);
    void NVMHTM_validate();

    // simulates a crash (SIGKILL)
    void NVMHTM_crash();

    // TODO: flush

#ifdef __cplusplus
}
#endif

#endif /* NVMHTM_H */
