#include "rdtsc.h"
#include "libnvmhtm.h"

#include <cstdio>
#include <cerrno>
#include <cstdlib>
#include <cstring>
#include <thread>
#include <vector>
#include <pthread.h>

GRANULE_TYPE *pool;

// TODO: crashes
#ifndef PROB_CRASH
#define PROB_CRASH 0.001
#endif

#define THREAD_SPACING 1
#define THREAD_OFFSET 0

int nb_accounts = 128, threads = 4, bank_budget = 5000,
        transfer_limit = 5, nb_transfers = 5000, nt_per_thread;
char *gnuplot_file;

void initialize_pool(GRANULE_TYPE *pool)
{
    int i;

    LIBNVMHTM_clear();

    for (i = 0; i < bank_budget; ++i) {
        int account = rand() % nb_accounts;
        int new_val = pool[account] + 1;

        pool[account] = new_val;
    }
    LIBNVMHTM_checkpoint();
}

int bank_total(GRANULE_TYPE *pool)
{
    int i, res = 0;

    for (i = 0; i < nb_accounts; ++i) {
        res += pool[i];
    }

    return res;
}

int bank_total_tx(GRANULE_TYPE *pool)
{
    int i, res = 0;

    LIBNVMHTM_start_tx();
    for (i = 0; i < nb_accounts; ++i) {
        res += pool[i];
    }
    LIBNVMHTM_commit_tx();

    return res;
}

void* random_transfer(void*)
{
    int tx;

    LIBNVMHTM_thr_init(); // inits the thread with the given pool

    for (tx = 0; tx < nt_per_thread; ++tx) {

        int transfer_amount = (rand() % transfer_limit) + 1; //
        int recipient = (rand() % nb_accounts);
        int sender = (rand() % nb_accounts);

        int local_counter = TM_get_local_counter(0);

        int sender_amount;
        int recipient_amount;
        int sender_new_amount;
        int recipient_new_amount;


        LIBNVMHTM_start_tx();

        sender_amount = pool[sender];
        recipient_amount = pool[recipient];
        sender_new_amount = sender_amount - transfer_amount;
        recipient_new_amount = recipient_amount + transfer_amount;

        if (sender_new_amount >= 0 && sender != recipient) {
            LIBNVMHTM_write(&(pool[sender]), sender_new_amount);
            LIBNVMHTM_write(&(pool[recipient]), recipient_new_amount);
        }

        LIBNVMHTM_commit_tx();
    }

    LIBNVMHTM_thr_exit();

    return NULL;
}

int main(int argc, char **argv)
{
    clock_t ts1, ts2;
    int i = 1;
    int reboot = 0;

    while (i < argc) {
        if (strcmp(argv[i], "REBOOT") == 0) {
            reboot = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "THREADS") == 0) {
            threads = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "NB_ACCOUNTS") == 0) {
            nb_accounts = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "NB_TRANSFERS") == 0) {
            nb_transfers = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "BANK_BUDGET") == 0) {
            bank_budget = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "TRANSFER_LIMIT") == 0) {
            threads = atoi(argv[i + 1]);
        }
        else if (strcmp(argv[i], "GNUPLOT_FILE") == 0) {
            gnuplot_file = strdup(argv[i + 1]);
        }
        i += 2;
    }

    nt_per_thread = nb_transfers / threads;

    // thread example
    printf(" Start program ========== \n");
    printf("         REBOOT: %i\n", reboot);
    printf("        THREADS: %i\n", threads);
    printf("    NB_ACCOUNTS: %i\n", nb_accounts);
    printf("    BANK_BUDGET: %i\n", bank_budget);
    printf("   NB_TRANSFERS: %i\n", nb_transfers);
    printf(" TRANSFER_LIMIT: %i\n", transfer_limit);
    printf(" ======================== \n");

    LIBNVMHTM_init(threads); // TODO: pass this to before the allocation

    pool = (GRANULE_TYPE*) LIBNVMHTM_alloc("./bank_accounts.dat",
                                           nb_accounts, 0);

    srand(clock()); // TODO: seed

    int total = bank_total(pool);
    if (reboot || total != bank_budget) {
        printf("Wrong bank amount: %i\n", total);
        initialize_pool(pool);
    }
    total = bank_total(pool);
    printf("Bank amount: %i\n", total);

    //    if (NVMHTM_is_crash(pool)) {
    //        printf("Is crash!\n");
    //        NVMHTM_recover(pool);
    //    }

    pthread_t pthrs[threads];
    pthread_attr_t attr[threads];
    cpu_set_t cpuset[threads];
    void *res;

    ts1 = clock();

    for (i = 0; i < threads; ++i) {
        CPU_ZERO(cpuset + i);
        CPU_SET(i * THREAD_SPACING + THREAD_OFFSET, cpuset + i);
        pthread_attr_init(&(attr[i]));
        pthread_attr_setaffinity_np(&(attr[i]), sizeof (cpu_set_t), &(cpuset[i]));

        pthread_create(&(pthrs[i]), &(attr[i]), random_transfer, NULL);
    }

    for (i = 0; i < threads; ++i) {
        pthread_join(pthrs[i], &res);
    }

    ts2 = clock();

    double time_taken = (double) (ts2 - ts1) / (double) CLOCKS_PER_SEC;
    int successes = TM_get_error_count(SUCCESS);
    int fallbacks = TM_get_error_count(FALLBACK);
    int aborts = TM_get_error_count(ABORT);
    int commits = successes + fallbacks;
    double X = (double) commits / (double) time_taken;
    double P_A = (double) aborts / (double) (successes + aborts);

    LIBNVMHTM_shutdown();

    printf("\n bank total: %i\n", bank_total(pool));

    if (gnuplot_file != NULL) {
        FILE *gp_fp = fopen(gnuplot_file, "a");

        if (ftell(gp_fp) < 8) {
            // not created yet

            fprintf(gp_fp, "#\t%s\t%s\t%s\t%s\t%s\t%s\t%s\n",
                    "THREADS", "NB_ACCOUNTS", "BANK_BUDGET", "NB_TRANSFERS",
                    "TRANSFER_LIMIT", "X", "P_A");
        }
        fprintf(gp_fp, "\t%i\t%i\t%i\t%i\t%i\t%e\t%e\n",
                threads, nb_accounts, bank_budget, nb_transfers,
                transfer_limit, X, P_A);
        fclose(gp_fp);
    }

    return EXIT_SUCCESS;
}
